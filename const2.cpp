#include <iostream>
using namespace std;

class c1
{
 const float f;
 const int i;
 static const float f2=4.0;
public:
 c1() : f(2.0), i(10) {}
 static const float f3 = 5.0;
 float get(){return f;}
};

// This program works but BS::section:10.4.6.1
// error: in-class not integral
// Only integral type can be const as member function: c++ standard.
// g++ -std=c++98 -pedantic => error: floating-point literal cannot appear in a constant-expression


//const float c1::f3 (10.0);
int main()
{
c1 obj1;
cout<<"\n" <<obj1.get() << "\n";

}

