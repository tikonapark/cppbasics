#include <iostream>
using namespace std;

//
// function as lvalue
//

int gi=0;

int& fn(){ return gi;}

int main()
{

++fn();
fn()++;

cout<<"\n" << gi << "\n";

fn() = 56;

cout<<"\n" << gi << "\n";

}

/*

in c if fn return pointer ti int then *fn() = 0; possible?
http://stackoverflow.com/questions/6111905/c-is-return-value-a-l-value

*/