#include <iostream>
using namespace std;

//
// BS:6.3.2.1, Declaration in conditions.
//


class c1 { int i;};

int main()
{

int i=0;

if (bool b=i)
{
 cout<<"\n"<< b << " " <<i << "\n";
}else
{
 b = 66;
 cout<<"\n" << b << "\n";
}

if (const int ci=66)
{
 cout<<"\n"<< ci << "\n";
}

c1 obj1;

//if (c1 obj2) error: expected primary-expression before âobj2â
{
 cout<<"\n"<< "nothing" << "\n";
}


}
