#include <iostream>
using namespace std;

//
// Variable used before declration..BS:10.2.9
//

class c1
{
 public:
 int fn(){ return i;}
 c1(int ii){i=ii;}


 private:
  int i;

};


int main()
{

int i=0;
c1 obj1(5);

cout<<"\n" << obj1.fn() << "\n";
}
