#if 1
#include <iostream>
using namespace std;
#else
#include <stdio.h>
#endif

// BS::section:4.1.1
// bool is fundamental type. not in C
// true is 1 when converted to integer, same for float?
// pointer converts easily to bool

int main()
{

bool b1; // gcc error:error: unknown type name ‘bool’
         // rename file bool.c otherwize gcc considering .cpp file differently.
bool b2 = true & false ; // bit wize AND

bool b3 = false + true;

bool b4 = true;
bool b5 = false;
bool b6 = b4 & b5;

float fl1 = b3;


#if 1
cout<<"\ntrue=" << true << "\n"; // o/p= ?
cout<<"\n" << b1 << "\n"; // o/p= ? 0xB7
cout<<"\n" << b2 << "\n";
cout<<"\n" << b3 << "\n";
cout<<"\n" << fl1 << "\n";
cout<<"\n" << b6 << "\n";
#else
printf ("\n%x\n",b1);
#endif
}
